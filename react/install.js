import { inject as openTemplateInject } from '@choerodon/master/lib/containers/components/c7n/routes/projectsPro/components/create-project/components/template-modal-inject';

import openTemplate from './injects/template-modal';

openTemplateInject('openTemplate', openTemplate);
